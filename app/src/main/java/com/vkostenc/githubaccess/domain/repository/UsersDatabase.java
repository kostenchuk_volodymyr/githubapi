package com.vkostenc.githubaccess.domain.repository;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "UsersDatabase")
public class UsersDatabase extends Model {
    @Column(name = "user")
    public String user;


  public static UsersDatabase get() {
    UsersDatabase usersDatabase = new Select().from(UsersDatabase.class).executeSingle();
    return usersDatabase;
  }

  public static UsersDatabase createNewUsersTable() {
    UsersDatabase usersDatabase = new UsersDatabase();
    usersDatabase.user = ",";
    return usersDatabase;
  }

  public static void clear() {
    new Delete().from(UsersDatabase.class).execute();
  }

}
