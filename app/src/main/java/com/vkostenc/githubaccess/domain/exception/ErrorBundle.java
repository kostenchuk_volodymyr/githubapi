package com.vkostenc.githubaccess.domain.exception;

public interface ErrorBundle {
  Exception getException();

  String getErrorMessage();
}
