package com.vkostenc.githubaccess.domain.repository;

import com.vkostenc.githubaccess.data.models.repository.RepositoryEntity;
import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.repository.datasource.user.UsersDataStore;
import com.vkostenc.githubaccess.domain.repository.datasource.user.UsersDataStoreFactory;

public class UsersDataRepository implements UsersDataStore {

  private static UsersDataRepository instance;

  public static UsersDataRepository getInstance() {
    if (instance == null) {
      synchronized (UsersDataRepository.class) {
        if (instance == null) {
          instance = new UsersDataRepository();
        }
      }
    }
    return instance;
  }

  private final UsersDataStoreFactory userDataStoreFactory;

  protected UsersDataRepository() {
    this.userDataStoreFactory = new UsersDataStoreFactory();
  }

  @Override public void getCurrentUserRepo(String userName, final UserRepoDataCallback userRepoDataCallback) {
    final UsersDataStore userDataStore = this.userDataStoreFactory.create();
    userDataStore.getCurrentUserRepo(userName, new UserRepoDataCallback() {
      @Override public void onUserRepoLoaded(RepositorysEntity repositorysEntity) {
        userRepoDataCallback.onUserRepoLoaded(repositorysEntity);
      }

      @Override public void onError(RepositoryErrorBundle error) {
        userRepoDataCallback.onError(error);
      }
    });
  }

  @Override public void login(String userName,
      final UserDataCallback userCallback) {
    final UsersDataStore userDataStore = this.userDataStoreFactory.create();
    userDataStore.login(userName, new UserDataCallback() {
      @Override public void onUserLoaded(UserEntity user) {
        userCallback.onUserLoaded(user);
      }

      @Override public void onError(RepositoryErrorBundle error) {
        userCallback.onError(error);
      }
    });
  }
}
