package com.vkostenc.githubaccess.domain.interactor.user;

import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.domain.exception.ErrorBundle;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.interactor.InteractorBase;
import com.vkostenc.githubaccess.domain.repository.UsersDataRepository;
import com.vkostenc.githubaccess.domain.repository.datasource.user.UsersDataStore;

public abstract class UsersBaseImpl extends InteractorBase {
  protected final UsersDataRepository userRepository;

  public UsersBaseImpl(PostExecutionThread postExecutionThread) {
    super(postExecutionThread);

    userRepository = UsersDataRepository.getInstance();
  }

  protected final UsersDataStore.UserDataCallback repositoryCallback =
      new UsersDataStore.UserDataCallback() {
        @Override public void onUserLoaded(UserEntity user) {
          notifyGetCurrentUserSuccess(user);
        }

        @Override public void onError(RepositoryErrorBundle error) {
          notifyError(error);
        }
      };

  private void notifyGetCurrentUserSuccess(final UserEntity user) {
    postExecutionThread.post(new Runnable() {
      @Override public void run() {
        ((UsersDataStore.UserDataCallback) callback).onUserLoaded(user);
      }
    });
  }

  private void notifyError(final ErrorBundle errorBundle) {
    postExecutionThread.post(new Runnable() {
      @Override public void run() {
        ((UsersDataStore.UserDataCallback) callback).onError((RepositoryErrorBundle) errorBundle);
      }
    });
  }
}
