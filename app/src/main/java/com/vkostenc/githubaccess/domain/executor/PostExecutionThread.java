package com.vkostenc.githubaccess.domain.executor;

public interface PostExecutionThread {
  void post(Runnable runnable);
}
