package com.vkostenc.githubaccess.domain.executor;

public interface ThreadExecutor {
  void execute(final Runnable runnable);
}
