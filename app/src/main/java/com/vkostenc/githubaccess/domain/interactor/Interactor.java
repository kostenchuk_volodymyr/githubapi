package com.vkostenc.githubaccess.domain.interactor;

public interface Interactor extends Runnable {
  void run();
}
