package com.vkostenc.githubaccess.domain.repository.datasource.user;

public class UsersDataStoreFactory {
  public UsersDataStore create() {
    UsersDataStore usersDataStore;

    usersDataStore = createCloudDataStore();

    return usersDataStore;
  }

  private UsersDataStore createCloudDataStore() {
    return new CloudUsersDataStore();
  }
}
