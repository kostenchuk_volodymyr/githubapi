package com.vkostenc.githubaccess.domain.repository.datasource.user;

import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.repository.datasource.RepositoryCallback;

public interface UsersDataStore {

  interface UserDataCallback extends RepositoryCallback {
    void onUserLoaded(UserEntity user);

    void onError(RepositoryErrorBundle error);
  }

  interface UserRepoDataCallback extends RepositoryCallback {
    void onUserRepoLoaded(RepositorysEntity repositorys);

    void onError(RepositoryErrorBundle error);
  }

  void getCurrentUserRepo(String userName, UserRepoDataCallback userRepoDataCallback);

  void login(String userName, UserDataCallback userCallback);
}
