package com.vkostenc.githubaccess.domain.repository.datasource.user;

import com.vkostenc.githubaccess.data.models.repository.RepositoryEntity;
import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.data.network.helpers.CredentialsHelper;
import com.vkostenc.githubaccess.data.network.services.RestApi;
import com.vkostenc.githubaccess.data.network.services.user.UsersServiceImpl;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import retrofit.RetrofitError;

public class CloudUsersDataStore implements UsersDataStore {

  @Override public void getCurrentUserRepo(String userName, final UserRepoDataCallback userRepoDataCallback) {

    UsersServiceImpl usersService = RestApi.getInstance().getUsersService();
    usersService.getCurrentUserRepo(userName, new UsersServiceImpl.RepoCallback() {
      @Override public void onRepoEntityLoaded(RepositorysEntity repositorysEntity) {
        userRepoDataCallback.onUserRepoLoaded(repositorysEntity);
      }

      @Override public void onError(RetrofitError error) {
        userRepoDataCallback.onError(new RepositoryErrorBundle(error));
      }
    });
  }
  @Override public void login(String userName,
      final UserDataCallback userCallback) {
    UsersServiceImpl usersService = RestApi.getInstance().getUsersService();
    usersService.login(userName, new UsersServiceImpl.UserCallback() {
      @Override public void onUserEntityLoaded(UserEntity userEntity) {
        userCallback.onUserLoaded(userEntity);

        CredentialsHelper.setCurrentUserCredentials(userEntity);
      }

      @Override public void onError(RetrofitError error) {
        userCallback.onError(new RepositoryErrorBundle(error));
      }
    });
  }

}
