package com.vkostenc.githubaccess.domain.interactor;

import com.vkostenc.githubaccess.domain.executor.JobExecutor;
import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.executor.ThreadExecutor;
import com.vkostenc.githubaccess.domain.repository.datasource.RepositoryCallback;

public abstract class InteractorBase implements Interactor {

  private final ThreadExecutor threadExecutor;
  protected final PostExecutionThread postExecutionThread;

  protected RepositoryCallback callback;

  public InteractorBase(PostExecutionThread postExecutionThread) {
    this.postExecutionThread = postExecutionThread;
    this.threadExecutor = JobExecutor.getInstance();
  }

  protected void executeBase(RepositoryCallback callback) {
    this.callback = callback;
    this.threadExecutor.execute(this);
  }
}
