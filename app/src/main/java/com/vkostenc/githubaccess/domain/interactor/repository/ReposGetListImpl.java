package com.vkostenc.githubaccess.domain.interactor.repository;

import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.domain.exception.ErrorBundle;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.interactor.InteractorBase;
import com.vkostenc.githubaccess.domain.repository.UsersDataRepository;

public class ReposGetListImpl extends InteractorBase {

  private final UsersDataRepository usersDataRepository;
  private String userName;

  public ReposGetListImpl(PostExecutionThread postExecutionThread) {
    super(postExecutionThread);

    usersDataRepository = UsersDataRepository.getInstance();
  }

  public void execute(UsersDataRepository.UserRepoDataCallback callback, String userName) {
    this.userName = userName;
    executeBase(callback);
  }

  @Override public void run() {
    usersDataRepository.getCurrentUserRepo(userName, repositoryCallback);
  }

  protected final UsersDataRepository.UserRepoDataCallback repositoryCallback =
      new UsersDataRepository.UserRepoDataCallback() {
        @Override public void onUserRepoLoaded(RepositorysEntity repositorys) {
          notifyGetListSuccess(repositorys);
        }

        @Override public void onError(RepositoryErrorBundle error) {
          notifyError(error);
        }
      };

  private void notifyGetListSuccess(final RepositorysEntity repositorys) {
    postExecutionThread.post(new Runnable() {
      @Override public void run() {
        ((UsersDataRepository.UserRepoDataCallback) callback).onUserRepoLoaded(repositorys);
      }
    });
  }

  private void notifyError(final ErrorBundle errorBundle) {
    postExecutionThread.post(new Runnable() {
      @Override public void run() {
        ((UsersDataRepository.UserRepoDataCallback) callback).onError(
            (RepositoryErrorBundle) errorBundle);
      }
    });
  }
}
