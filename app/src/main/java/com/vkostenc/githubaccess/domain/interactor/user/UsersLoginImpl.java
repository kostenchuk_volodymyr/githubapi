package com.vkostenc.githubaccess.domain.interactor.user;

import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.repository.datasource.user.UsersDataStore;

public class UsersLoginImpl extends UsersBaseImpl {

  private String userName;

  public UsersLoginImpl(PostExecutionThread postExecutionThread) {
    super(postExecutionThread);
  }

  public void execute(UsersDataStore.UserDataCallback callback, String userName) {
    this.userName = userName;
    executeBase(callback);
  }

  @Override public void run() {
    userRepository.login(userName, repositoryCallback);
  }
}
