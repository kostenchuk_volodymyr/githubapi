package com.vkostenc.githubaccess.presentation.view.base.authorization;

import com.vkostenc.githubaccess.presentation.view.base.EditView;

public interface UserLoginEditView extends EditView {
  void loginDataError(String message);

  void loginDataAccepted();
  void dataValidated();
}
