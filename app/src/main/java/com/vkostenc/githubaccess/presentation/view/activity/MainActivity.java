package com.vkostenc.githubaccess.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.data.network.helpers.CredentialsHelper;
import com.vkostenc.githubaccess.data.network.helpers.CurrentUserId;
import com.vkostenc.githubaccess.data.network.services.ApiConstants;
import com.vkostenc.githubaccess.domain.repository.UsersDatabase;
import com.vkostenc.githubaccess.presentation.navigation.Navigator;
import com.vkostenc.githubaccess.presentation.view.fragment.authorization.MainFragment;
import com.vkostenc.githubaccess.presentation.view.fragment.authorization.UserRequestBaseFragment;

public class MainActivity extends ToolbarActivity {

  private Navigator navigator;
  private UsersDatabase usersDatabase;

  public static Intent getCallingIntent(Context context) {
    return new Intent(context, MainActivity.class);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_toolbar);
    initialize();
  }

  private void initialize() {
    navigator = new Navigator();

    MainFragment mainFragment = MainFragment.newInstance();

    addFragment(R.id.fl_main, mainFragment);
    initToolbar(CurrentUserId.getInstance().getCurrentUserLogin());

    usersDatabase = UsersDatabase.get();
    if (usersDatabase == null){
      usersDatabase = UsersDatabase.createNewUsersTable();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_pull_down, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        navigateToLoginScreen();
        break;
      case R.id.item_open_in_browser:
        openInBrowser();
        break;
      case R.id.item_save:
        saveToDB();
        break;
      case R.id.item_clear:
        clearFromDB();
        break;
      case R.id.item_share:
        shareOption();
        break;
    }
    return true;
  }

  private void clearFromDB() {
    UsersDatabase.clear();
  }

  private void saveToDB() {
    if (!usersDatabase.user.contains(","+CurrentUserId.getInstance().getCurrentUserLogin()+",")){
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(usersDatabase.user);
      stringBuilder.append(CurrentUserId.getInstance().getCurrentUserLogin()+",");
      usersDatabase.user = stringBuilder.toString();
      usersDatabase.save();
    }
  }

  private void shareOption() {
    navigator.navigateToShareOption(this, String.format(getString(R.string.fragment_main_share_message),
        ApiConstants.BASE_BROWSER_URL + CurrentUserId.getInstance().getCurrentUserLogin()),
        getString(R.string.fragment_main_share_title));
  }

  private void openInBrowser() {
    navigator.navigateToBrowser(this, ApiConstants.BASE_BROWSER_URL +
        CurrentUserId.getInstance().getCurrentUserLogin());
  }

  @Override
  public void onBackPressed() {
    navigateToLoginScreen();
  }

  private void navigateToLoginScreen() {
    CredentialsHelper.logout();
    finish();
    navigator.navigateToLoginScreen(MainActivity.this);
  }

}
