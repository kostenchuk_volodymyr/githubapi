package com.vkostenc.githubaccess.presentation.mapper.repository;

import com.vkostenc.githubaccess.data.models.repository.RepositoryEntity;
import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.presentation.model.repository.RepositoryModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class RepoDataMapper {
  public RepositoryModel transform(RepositoryEntity repositoryEntity){
    RepositoryModel repositoryModel = new RepositoryModel();
    repositoryModel.setName(repositoryEntity.name);
    repositoryModel.setLanguage(repositoryEntity.language);
    repositoryModel.setBranches(repositoryEntity.branches);
    repositoryModel.setStargazersCount(repositoryEntity.stargazers_count);
    return repositoryModel;
  }


  private Collection<RepositoryModel> transform(
      Collection<RepositoryEntity> repossCollection) {
    Collection<RepositoryModel> repoModelCollection;

    if (repossCollection != null && !repossCollection.isEmpty()) {
      repoModelCollection = new ArrayList<>();
      for (RepositoryEntity repositoryEntity : repossCollection) {
        repoModelCollection.add(transform(repositoryEntity));
      }
    } else {
      repoModelCollection = Collections.emptyList();
    }

    return repoModelCollection;
  }

  public Collection<RepositoryModel> transform(RepositorysEntity repositorysEntity) {
    Collection<RepositoryModel> repoModelCollection = null;

    if (repositorysEntity != null) {
      RepositoryEntity[] repositoryEntities = repositorysEntity.repositoryEntities;

      if (repositoryEntities != null) {
        repoModelCollection = transform(Arrays.asList(repositoryEntities));
      }
    }

    if (repoModelCollection == null) {
      repoModelCollection = Collections.emptyList();
    }

    return repoModelCollection;
  }

}
