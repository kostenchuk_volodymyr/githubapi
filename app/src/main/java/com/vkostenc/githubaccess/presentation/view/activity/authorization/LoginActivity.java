package com.vkostenc.githubaccess.presentation.view.activity.authorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.presentation.model.user.UserModel;
import com.vkostenc.githubaccess.presentation.navigation.Navigator;
import com.vkostenc.githubaccess.presentation.view.activity.BaseActivity;
import com.vkostenc.githubaccess.presentation.view.fragment.authorization.LoginFragment;
import com.vkostenc.githubaccess.presentation.view.fragment.authorization.UserRequestBaseFragment;
import com.vkostenc.githubaccess.presentation.view.helper.DialogHelper;

public class LoginActivity extends BaseActivity {

  private Navigator navigator;
  private AlertDialog appExit;

  public static Intent getCallingIntent(Context context) {
    return new Intent(context, LoginActivity.class);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_no_toolbar);

    initialize();
  }

  private void initialize() {
    navigator = new Navigator();

    LoginFragment loginFragment = LoginFragment.newInstance();
    loginFragment.setOnCurrentUserRequestListener(
        new UserRequestBaseFragment.OnCurrentUserRequestListener() {
          @Override public void onUserLoadSucceed() {
            navigateToMainScreen();
          }

          @Override public void onUserLoadFailed() {}
        });
    addFragment(R.id.fl_main, loginFragment);
  }

  private void navigateToMainScreen() {
    finish();
    navigator.navigateToMainScreen(LoginActivity.this);
  }

  @Override
  public void onBackPressed() {

    appExit = DialogHelper.constructAlertDialogWithYesNoButton(LoginActivity.this,
        getString(R.string.activity_login_exit_title),
        getString(R.string.activity_login_exit_description),
        new DialogHelper.OnDialogYesNoButtonClickedListener() {
          @Override public void onYesClick() {
            appExit.dismiss();
            finish();
          }

          @Override public void onNoClick() {
            appExit.dismiss();
          }
        });
    appExit.show();
  }
}
