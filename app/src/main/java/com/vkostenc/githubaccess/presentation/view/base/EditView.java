package com.vkostenc.githubaccess.presentation.view.base;

import android.content.Context;

public interface EditView {
  Context getContext();
}
