package com.vkostenc.githubaccess.presentation.view.fragment.authorization;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.data.network.helpers.CurrentUserId;
import com.vkostenc.githubaccess.presentation.model.repository.RepositoryModel;
import com.vkostenc.githubaccess.presentation.presenter.MainListPresenter;
import com.vkostenc.githubaccess.presentation.view.adapter.ListLayoutManager;
import com.vkostenc.githubaccess.presentation.view.adapter.MainAdapter;
import com.vkostenc.githubaccess.presentation.view.base.ReposLoadView;
import com.vkostenc.githubaccess.presentation.view.fragment.BaseListFragment;
import java.util.List;

public class MainFragment extends BaseListFragment implements ReposLoadView {

  private MainListPresenter mainListPresenter;
  private MainAdapter mainAdapter;
  private ViewGroup topContainer;

  public static MainFragment newInstance() {
    MainFragment mainFragment = new MainFragment();
    Bundle argumentsBundle = new Bundle();
    mainFragment.setArguments(argumentsBundle);
    return mainFragment;
  }

  @Override protected String getEmptyText() {
    return getContext().getString(R.string.fragment_repos_list_empty_text);
  }


  @Override protected void reloadData() {
    MainFragment.this.mainListPresenter.loadData();
  }

  @Override protected void onScrolled(RecyclerView recyclerView, int dx, int dy,
      ListLayoutManager layoutManager) {
    
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    this.mainListPresenter.initialize();
  }

  @Override public void onResume() {
    super.onResume();
    this.mainListPresenter.resume(CurrentUserId.getInstance().getCurrentUserLogin());
  }

  @Override public void onPause() {
    super.onPause();
    this.mainListPresenter.pause();
  }

  @Override protected void initializePresenter() {
    this.mainListPresenter =
        new MainListPresenter(this);
  }

  @Override public void reposLoaded(List<RepositoryModel> repositoryModels, boolean isInitialLoad) {

    if (repositoryModels != null) {
          if (this.mainAdapter == null) {
            this.mainAdapter = new MainAdapter(getActivity(), repositoryModels);
            setAdapter(mainAdapter);
          } else {
            this.mainAdapter.setReposCollection(repositoryModels);
          }
        }
  }
}
