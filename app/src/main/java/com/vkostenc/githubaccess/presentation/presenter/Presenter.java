package com.vkostenc.githubaccess.presentation.presenter;

public interface Presenter {
  void resume(String userName);

  void pause();
}
