package com.vkostenc.githubaccess.presentation.presenter.authorization;

import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.domain.exception.ErrorBundle;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.interactor.user.UsersLoginImpl;
import com.vkostenc.githubaccess.domain.repository.datasource.user.UsersDataStore;
import com.vkostenc.githubaccess.presentation.exception.ErrorMessageFactory;
import com.vkostenc.githubaccess.presentation.executor.UIThread;
import com.vkostenc.githubaccess.presentation.view.base.authorization.UserLoadView;
import com.vkostenc.githubaccess.presentation.view.base.authorization.UserLoginEditView;

public class LoginPresenter extends LoginEditPresenter {

  private final UserLoadView viewDetailsView;
  private final UsersLoginImpl login;

  public LoginPresenter(UserLoadView userDetailsView, UserLoginEditView userLoginEditView) {
    super(userLoginEditView);
    PostExecutionThread postExecutionThread = UIThread.getInstance();
    this.login = new UsersLoginImpl(postExecutionThread);
    this.viewDetailsView = userDetailsView;
  }

  public void initialize(String username) {
    this.loadCurrentUser(username);
  }

  private void loadCurrentUser(String username) {
    this.showViewLoading();
    this.getUserDetails(username);
  }

  private void showViewLoading() {
    this.viewDetailsView.showLoading();
  }

  private void hideViewLoading() {
    this.viewDetailsView.hideLoading();
  }

  private void showErrorMessage(ErrorBundle errorBundle) {
    String errorMessage = ErrorMessageFactory.create(this.viewDetailsView.getContext(),
        errorBundle.getException());
    this.viewDetailsView.showError(errorMessage);
  }

  private void userLoaded(UserEntity user) {
    this.viewDetailsView.userLoaded();
  }

  private void getUserDetails(String username) {
    this.login.execute(this.userDetailsCallback, username);
  }

  private final UsersDataStore.UserDataCallback userDetailsCallback =
      new UsersDataStore.UserDataCallback() {
        @Override public void onUserLoaded(UserEntity user) {
          LoginPresenter.this.hideViewLoading();
          LoginPresenter.this.userLoaded(user);
        }

        @Override public void onError(RepositoryErrorBundle error) {
          LoginPresenter.this.hideViewLoading();
          LoginPresenter.this.showErrorMessage(error);
        }
      };

  public void validateData(String userName) {
    if(checkLogin(userName))
    {
      this.viewDetailsView.dataValidated();
    }
  }
}
