package com.vkostenc.githubaccess.presentation.presenter.authorization;

import com.vkostenc.githubaccess.presentation.presenter.Presenter;
import com.vkostenc.githubaccess.presentation.utils.Validator;
import com.vkostenc.githubaccess.presentation.view.base.authorization.UserLoginEditView;

public abstract class LoginEditPresenter {

  private UserLoginEditView userLoginEditView;

  public LoginEditPresenter(UserLoginEditView userLoginEditView) {
    this.userLoginEditView = userLoginEditView;
  }

  public boolean checkLogin(String login) {
    String error = Validator.validateLogin(userLoginEditView.getContext(), login);

    if (error == null) {
      userLoginEditView.loginDataAccepted();
      return true;
    } else {
      userLoginEditView.loginDataError(error);
      return false;
    }
  }
}
