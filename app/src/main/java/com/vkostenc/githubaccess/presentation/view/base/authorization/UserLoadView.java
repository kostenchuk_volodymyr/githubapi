package com.vkostenc.githubaccess.presentation.view.base.authorization;

import com.vkostenc.githubaccess.presentation.model.user.UserModel;
import com.vkostenc.githubaccess.presentation.view.base.LoadDataView;

public interface UserLoadView extends LoadDataView {
  void userLoaded();
}
