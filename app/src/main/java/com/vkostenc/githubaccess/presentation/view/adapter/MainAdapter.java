package com.vkostenc.githubaccess.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.presentation.model.repository.RepositoryModel;
import java.util.Collection;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {

  public interface OnItemClickListener {
    void onMainItemClicked(RepositoryModel repositoryModel);
  }

  private OnItemClickListener onItemClickListener;

  private List<RepositoryModel> reposCollection;
  private LayoutInflater layoutInflater;
  private final Context context;

  public MainAdapter(Context context, Collection<RepositoryModel> reposCollection) {
    this.context = context;
    if (context != null) {
      this.layoutInflater =
          (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    this.reposCollection = (List<RepositoryModel>) reposCollection;
  }

  @Override public int getItemCount() {
    return (this.reposCollection != null) ? this.reposCollection.size() : 0;
  }

  @Override public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (this.layoutInflater != null) {
      View view =
          this.layoutInflater.inflate(R.layout.recycler_view_item_repo, parent, false);

      return new MainViewHolder(view);
    }

    return null;
  }

  @Override public void onBindViewHolder(MainViewHolder holder, final int position) {
    final RepositoryModel repositoryModel = this.reposCollection.get(position);

    holder.tv_repo_title.setText(repositoryModel.getName());
    holder.tv_repo_language.setText(repositoryModel.getLanguage());
    holder.tv_repo_branches.setText(String.valueOf(repositoryModel.getBranches()));
    holder.tv_repo_stars.setText(String.valueOf(repositoryModel.getStargazersCount()));
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    this.onItemClickListener = onItemClickListener;
  }

  @Override public long getItemId(int position) {
    return position;
  }

  public void setReposCollection(Collection<RepositoryModel> reposCollection) {
    this.reposCollection = (List<RepositoryModel>) reposCollection;
    this.notifyDataSetChanged();
  }

  static class MainViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.tv_repo_title) TextView tv_repo_title;
    @Bind(R.id.tv_repo_language) TextView tv_repo_language;
    @Bind(R.id.tv_repo_branches) TextView tv_repo_branches;
    @Bind(R.id.tv_repo_stars) TextView tv_repo_stars;

    public MainViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}