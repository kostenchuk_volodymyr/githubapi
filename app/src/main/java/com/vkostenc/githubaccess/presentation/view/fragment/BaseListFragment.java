package com.vkostenc.githubaccess.presentation.view.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.data.network.helpers.CurrentUserId;
import com.vkostenc.githubaccess.presentation.view.adapter.ListLayoutManager;
import com.vkostenc.githubaccess.presentation.view.base.LoadListDataView;

public abstract class BaseListFragment extends BaseFragment implements LoadListDataView {

  @Bind(R.id.iv_user_photo) ImageView iv_user_photo;
  @Bind(R.id.tv_fr_main_name) TextView tv_fr_main_name;
  @Bind(R.id.tv_fr_main_followers) TextView tv_fr_main_followers;
  @Bind(R.id.tv_fr_main_following) TextView tv_fr_main_following;
  @Bind(R.id.tv_fr_main_gists) TextView tv_fr_main_gists;
  @Bind(R.id.tv_fr_main_repos) TextView tv_fr_main_repos;

  @Bind(R.id.srl_container) SwipeRefreshLayout srl_container;
  @Bind(R.id.rv_list) RecyclerView rv_list;

  @Bind(R.id.rl_main) RelativeLayout rl_main;
  @Bind(R.id.rl_progress) RelativeLayout rl_progress;
  @Bind(R.id.rl_retry) RelativeLayout rl_retry;
  @Bind(R.id.rl_empty) RelativeLayout rl_empty;

  private ListLayoutManager listLayoutManager;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
    ButterKnife.bind(this, fragmentView);

    initializeHeader();
    initializeList();
    initializeSwipeRefreshLayout();
    initializeRetry();
    initializeEmptyView();

    return fragmentView;
  }

  private void initializeEmptyView() {
    TextView emptyText = (TextView) rl_empty.findViewById(R.id.tv_empty_text);
    emptyText.setText(getEmptyText());
  }

  protected abstract String getEmptyText();

  protected abstract void reloadData();

  private void initializeHeader(){
    Picasso.with(getActivity())
        .load(CurrentUserId.getInstance().getCurrentUserAvatarUrl())
        .placeholder(R.drawable.ic_def_user)
        .error(R.drawable.ic_def_user)
        .fit()
        .into(iv_user_photo);

    StringBuilder nameTitle = new StringBuilder();
    if (!TextUtils.isEmpty(CurrentUserId.getInstance().getCurrentUserName())){
      nameTitle.append(CurrentUserId.getInstance().getCurrentUserName() + ", ");
    }
    if (!TextUtils.isEmpty(CurrentUserId.getInstance().getCurrentUserCompany())){
      nameTitle.append(CurrentUserId.getInstance().getCurrentUserCompany() + ", ");
    }
    if (!TextUtils.isEmpty(CurrentUserId.getInstance().getCurrentUserEmail())){
      nameTitle.append(CurrentUserId.getInstance().getCurrentUserEmail());
    }

    tv_fr_main_name.setText(nameTitle.toString());

    String formatFollowers = String.format(getString(R.string.fragment_main_followers),
        CurrentUserId.getInstance().getCurrentUserFollowers());

    String formatFollowing = String.format(getString(R.string.fragment_main_following),
        CurrentUserId.getInstance().getCurrentUserFollowing());

    String formatGists = String.format(getString(R.string.fragment_main_gists),
        CurrentUserId.getInstance().getCurrentUserPublicGists());

    String formatRepos = String.format(getString(R.string.fragment_main_repos),
        CurrentUserId.getInstance().getCurrentUserPublicRepos());

    tv_fr_main_followers.setText(formatFollowers);
    tv_fr_main_following.setText(formatFollowing);
    tv_fr_main_gists.setText(formatGists);
    tv_fr_main_repos.setText(formatRepos);
  }

  protected void setAdapter(RecyclerView.Adapter adapter) {
    this.rv_list.setAdapter(adapter);
  }

  private void initializeRetry() {
    rl_retry.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        reloadData();
      }
    });
  }

  private void initializeSwipeRefreshLayout() {
    srl_container.setColorSchemeColors(getResources().getColor(R.color.pink));
    srl_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        reloadData();
      }
    });
  }

  private void initializeList() {
    listLayoutManager = new ListLayoutManager(getActivity());
    this.rv_list.setLayoutManager(listLayoutManager);

    this.rv_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        BaseListFragment.this.onScrolled(recyclerView, dx, dy, listLayoutManager);
      }
    });
  }

  protected abstract void onScrolled(RecyclerView recyclerView, int dx, int dy,
      ListLayoutManager layoutManager);

  @Override public void showLoading() {
    srl_container.setRefreshing(true);
  }

  @Override public void hideLoading() {
    srl_container.setRefreshing(false);
  }

  @Override public void showMainLayout() {
    rl_main.setVisibility(View.VISIBLE);
  }

  @Override public void hideMainLayout() {
    rl_main.setVisibility(View.GONE);
  }

  @Override public void showLargeProgress() {
    rl_progress.setVisibility(View.VISIBLE);
  }

  @Override public void hideLargeProgress() {
    rl_progress.setVisibility(View.GONE);
  }

  @Override public void showSmallError(String message) {
    showToastMessage(message);
  }

  @Override public void showRetry() {
    rl_retry.setVisibility(View.VISIBLE);
  }

  @Override public void hideRetry() {
    rl_retry.setVisibility(View.GONE);
  }

  @Override public void showError(String message) {

  }

  @Override public void dataValidated() {

  }

  @Override public void showEmptyLayout() {
    rl_empty.setVisibility(View.VISIBLE);
  }

  @Override public void hideEmptyLayout() {
    rl_empty.setVisibility(View.GONE);
  }
}
