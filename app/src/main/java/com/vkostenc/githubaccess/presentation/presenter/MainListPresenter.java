package com.vkostenc.githubaccess.presentation.presenter;

import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.network.helpers.CurrentUserId;
import com.vkostenc.githubaccess.domain.exception.ErrorBundle;
import com.vkostenc.githubaccess.domain.exception.RepositoryErrorBundle;
import com.vkostenc.githubaccess.domain.executor.PostExecutionThread;
import com.vkostenc.githubaccess.domain.interactor.repository.ReposGetListImpl;
import com.vkostenc.githubaccess.domain.repository.UsersDataRepository;
import com.vkostenc.githubaccess.presentation.exception.ErrorMessageFactory;
import com.vkostenc.githubaccess.presentation.executor.UIThread;
import com.vkostenc.githubaccess.presentation.mapper.repository.RepoDataMapper;
import com.vkostenc.githubaccess.presentation.model.repository.RepositoryModel;
import com.vkostenc.githubaccess.presentation.view.base.ReposLoadView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MainListPresenter extends ListBasePresenter {

  private final ReposLoadView reposLoadView;

  private final RepoDataMapper repoDataMapper;

  private final ReposGetListImpl reposGetList;

  public MainListPresenter(ReposLoadView reposLoadView) {
    super(reposLoadView);

    PostExecutionThread postExecutionThread = UIThread.getInstance();
    this.reposGetList = new ReposGetListImpl(postExecutionThread);

    this.repoDataMapper = new RepoDataMapper();

    this.reposLoadView = reposLoadView;
  }

  public void initialize() {
    initializeListPresenter();
    loadData();
  }

  public void loadData() {
    loadListFromScratch(CurrentUserId.getInstance().getCurrentUserLogin());
  }

  @Override
  protected void invokeLoadListMethods(String userName) {
    this.getReposList(userName);
  }

  private void showErrorMessage(ErrorBundle errorBundle) {
    String errorMessage = ErrorMessageFactory.create(this.reposLoadView.getContext(),
        errorBundle.getException());
    this.reposLoadView.showError(errorMessage);

    loadingFailed(errorMessage);
  }

  private void reposLoaded(RepositorysEntity repositoryEntity) {
    Collection<RepositoryModel> collection =
        this.repoDataMapper.transform(repositoryEntity);
    final List<RepositoryModel> repoModelList =
        new ArrayList<>(collection);
    this.reposLoadView.reposLoaded(repoModelList, isNeedToUpdateView());
    loadingSucceed(repoModelList.size());
  }

  private void getReposList(String userName) {
    this.reposGetList.execute(this.reposDataCallback, userName);
  }

  private final UsersDataRepository.UserRepoDataCallback reposDataCallback =
      new UsersDataRepository.UserRepoDataCallback() {
        @Override public void onUserRepoLoaded(RepositorysEntity repositorysEntity) {
          MainListPresenter.this.reposLoaded(repositorysEntity);
        }

        @Override public void onError(RepositoryErrorBundle error) {
          MainListPresenter.this.showErrorMessage(error);
        }
      };
}