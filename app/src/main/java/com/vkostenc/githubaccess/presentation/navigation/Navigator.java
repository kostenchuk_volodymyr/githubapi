package com.vkostenc.githubaccess.presentation.navigation;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.vkostenc.githubaccess.presentation.view.activity.MainActivity;
import com.vkostenc.githubaccess.presentation.view.activity.authorization.LoginActivity;

public class Navigator {

  public void navigateToLoginScreen(Activity activity) {
    if (activity != null) {
      Intent intentToLaunch = LoginActivity.getCallingIntent(activity);
      activity.startActivity(intentToLaunch);
    }
  }

  public void navigateToMainScreen(Activity activity) {
    if (activity != null) {
      Intent intentToLaunch = MainActivity.getCallingIntent(activity);
      activity.startActivity(intentToLaunch);
    }
  }

  public void navigateToBrowser(Activity activity, String path) {
    if (activity != null) {
      Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
      activity.startActivity(browserIntent);
    }
  }

  public void navigateToShareOption(Activity activity, String message, String title) {
    if (activity != null) {
      Intent intent = new Intent(android.content.Intent.ACTION_SEND);
      intent.setType("text/plain");
      intent.putExtra(android.content.Intent.EXTRA_TEXT, message);
      activity.startActivity(
          Intent.createChooser(intent, title));
    }
  }

}
