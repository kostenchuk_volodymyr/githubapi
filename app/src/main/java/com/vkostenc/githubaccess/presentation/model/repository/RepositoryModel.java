package com.vkostenc.githubaccess.presentation.model.repository;

import com.vkostenc.githubaccess.data.models.repository.Branch;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import java.util.Date;
import java.util.List;

public class RepositoryModel {

  private boolean fork;
  private boolean isPrivate;
  private Date created_at;
  private Date pushed_at;
  private Date updated_at;
  private int forks_count;
  private long id;
  private RepositoryModel parent;
  private RepositoryModel source;
  private String clone_url;
  private String description;
  private String homepage;
  private String git_url;
  private String language;
  private String default_branch;
  private String mirror_url;
  private String name;
  private String full_name;
  private String ssh_url;
  private String svn_url;
  private UserEntity owner;
  private int stargazers_count;
  private int subscribers_count;
  private int network_count;
  private int watchers_count;
  private int size;
  private int open_issues_count;
  private boolean has_issues;
  private boolean has_downloads;
  private boolean has_wiki;
  private Permissions permissions;
  private License license;
  private List<Branch> branches;
  private String archive_url;

  private class Permissions {
    public boolean admin;
    public boolean push;
    public boolean pull;
  }

  private class License {
    public String key;
    public String name;
    public String url;
    public boolean featured;
  }


  public String getName() {
    return name;
  }
  public String getLanguage() {
    return language;
  }
  public int getBranches() {
    int size = 0;
    if (branches != null && !branches.isEmpty()){
      size = branches.size();
    }
    return size;
  }
  public int getStargazersCount() {
    return stargazers_count;
  }

  public void setName(String name) {
    this.name = name;
  }
  public void setLanguage(String language) {
    this.language = language;
  }
  public void setBranches(List<Branch> branches ) {
    this.branches = branches;
  }
  public void setStargazersCount(int stargazers_count) {
    this.stargazers_count = stargazers_count;
  }
}


