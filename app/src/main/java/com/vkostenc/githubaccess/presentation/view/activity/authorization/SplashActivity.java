package com.vkostenc.githubaccess.presentation.view.activity.authorization;

import android.os.Bundle;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.presentation.navigation.Navigator;
import com.vkostenc.githubaccess.presentation.view.activity.BaseActivity;
import com.vkostenc.githubaccess.presentation.view.fragment.authorization.SplashFragment;

public class SplashActivity extends BaseActivity {

  private Navigator navigator;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_no_toolbar);

    initialize();
  }

  private void initialize() {
    navigator = new Navigator();

    SplashFragment splashFragment = SplashFragment.newInstance();
    splashFragment.setOnCurrentUserRequestListener(
        new SplashFragment.OnCurrentUserRequestListener() {
          @Override public void onUserLoadSucceed() {
            navigateToMainScreen();
          }

          @Override public void onUserLoadFailed() {
            navigateToLoginScreen();
          }
        });
    addFragment(R.id.fl_main, splashFragment);
  }

  private void navigateToMainScreen() {
    finish();
    navigator.navigateToMainScreen(SplashActivity.this);
  }

  private void navigateToLoginScreen() {
    finish();
    navigator.navigateToLoginScreen(SplashActivity.this);
  }
}
