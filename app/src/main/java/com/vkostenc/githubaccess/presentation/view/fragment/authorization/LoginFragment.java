package com.vkostenc.githubaccess.presentation.view.fragment.authorization;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.domain.repository.UsersDatabase;
import com.vkostenc.githubaccess.presentation.model.user.UserModel;
import com.vkostenc.githubaccess.presentation.presenter.authorization.LoginPresenter;
import com.vkostenc.githubaccess.presentation.view.base.authorization.UserLoadView;
import com.vkostenc.githubaccess.presentation.view.base.authorization.UserLoginEditView;
import com.vkostenc.githubaccess.presentation.view.helper.AutoSuggestAdapter;
import com.vkostenc.githubaccess.presentation.view.helper.DialogHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginFragment extends UserRequestBaseFragment implements UserLoadView, UserLoginEditView {

  private LoginPresenter loginPresenter;
  private ProgressDialog loginProgressDialog;
  private AlertDialog loginErrorAlertDialog;
  private UsersDatabase usersDatabase;

  @Bind(R.id.et_username) AutoCompleteTextView et_username;

  public static LoginFragment newInstance() {
    LoginFragment loginFragment = new LoginFragment();
    Bundle argumentsBundle = new Bundle();
    loginFragment.setArguments(argumentsBundle);
    return loginFragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initialize();
  }

  @Override protected void initializePresenter() {
    loginPresenter = new LoginPresenter(this, this);
  }

  private void initialize() {
    usersDatabase = UsersDatabase.get();
    if (usersDatabase == null){
      usersDatabase = UsersDatabase.createNewUsersTable();
    }

    loginProgressDialog = DialogHelper.constructProgressDialogWithSpinner(getActivity(),
        getContext().getString(R.string.fragment_login_authenticating),
        getContext().getString(R.string.fragment_login_please_wait_for_server_response));
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);
    ButterKnife.bind(this, fragmentView);

    return fragmentView;
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (!TextUtils.isEmpty(usersDatabase.user)){
      String savedUsers = usersDatabase.user;
      List <String> usersList = new ArrayList<String>(Arrays.asList(savedUsers.split(",")));
      AutoSuggestAdapter adapter = new AutoSuggestAdapter(this.getContext(), android.R.layout.simple_list_item_1, usersList);
      et_username.setAdapter(adapter);
      et_username.setThreshold(1);
    }
  }

    @Override public void userLoaded() {
      notifyUserLoadSucceed();
    }

    @Override public void showError(String message) {
      loginErrorAlertDialog = DialogHelper.constructAlertDialogWithOkButton(getActivity(),
          getContext().getString(R.string.fragment_login_authentication_error), message,
          new DialogHelper.OnDialogButtonClickedListener() {
            @Override public void onOkClick() {
              loginErrorAlertDialog.cancel();
            }
          });

      loginErrorAlertDialog.show();
    }

  @Override public void dataValidated() {
    LoginFragment.this.login();
  }

  @Override public void showLoading() {
    loginProgressDialog.show();
  }

  @Override public void hideLoading() {
    loginProgressDialog.cancel();
  }

  @Override public void showRetry() {  }

  @Override public void hideRetry() {  }


  @OnClick(R.id.tv_get_account) void onButtonLoginClick() {
    LoginFragment.this.validateData();
  }

  private void validateData() {
    if (loginPresenter != null && et_username != null) {
      loginPresenter.validateData(et_username.getText().toString());
    }
  }

  @OnTextChanged(R.id.et_username) void onLoginTextChanged() {
    LoginFragment.this.checkLogin();
  }

  private void checkLogin() {
    if (loginPresenter != null && et_username != null) {
      loginPresenter.checkLogin(et_username.getText().toString());
    }
  }

  private void login() {
    if (loginPresenter != null && et_username != null) {
      if (et_username.getError() == null) {
        loginPresenter.initialize(et_username.getText().toString());
      }
    }
  }

  @Override public void loginDataError(String message) {
    et_username.setError(message);
  }

  @Override public void loginDataAccepted() {
    et_username.setError(null);
  }
}
