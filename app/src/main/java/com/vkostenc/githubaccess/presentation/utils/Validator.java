package com.vkostenc.githubaccess.presentation.utils;

import android.content.Context;
import com.vkostenc.githubaccess.R;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

  private final static int MIN_LENGTH = 1;
  private final static int MAX_LENGTH = 255;

  public static String validateLogin(Context context, String login) {
    String error = null;

    if (login.length() < MIN_LENGTH) {
      error = context.getString(R.string.validator_login_is_required);
    } else if (login.length() > MAX_LENGTH) {
      error = context.getString(R.string.validator_login_is_to_long);
    } else if (!login.matches("^[0-9a-zA-Z]*$")) {
      error = context.getString(R.string.validator_characters_not_allowed);
    }
    return error;
  }
}
