package com.vkostenc.githubaccess.presentation.view.base;

import com.vkostenc.githubaccess.presentation.model.repository.RepositoryModel;
import java.util.List;

public interface ReposLoadView extends LoadListDataView {
  void reposLoaded(List<RepositoryModel> repositoryModels, boolean isInitialLoad);
}
