package com.vkostenc.githubaccess.presentation.view.fragment.authorization;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.vkostenc.githubaccess.R;
import com.vkostenc.githubaccess.data.network.helpers.CurrentUserId;

public class SplashFragment extends UserRequestBaseFragment{


  @Bind(R.id.rl_progress) RelativeLayout rl_progress;

  public static SplashFragment newInstance() {
    SplashFragment splashFragment = new SplashFragment();

    Bundle argumentsBundle = new Bundle();
    splashFragment.setArguments(argumentsBundle);

    return splashFragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override protected void initializePresenter() {

  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View fragmentView = inflater.inflate(R.layout.fragment_splash, container, false);
    ButterKnife.bind(this, fragmentView);

    return fragmentView;
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    this.rl_progress.setVisibility(View.VISIBLE);

    if (TextUtils.isEmpty(CurrentUserId.getInstance().getCurrentUserLogin())){
      notifyUserLoadFailed();
    }else {
      notifyUserLoadSucceed();
    }

    this.rl_progress.setVisibility(View.GONE);
  }
}
