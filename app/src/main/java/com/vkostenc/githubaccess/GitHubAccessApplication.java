package com.vkostenc.githubaccess;

import android.app.Application;
import android.content.Context;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.vkostenc.githubaccess.data.network.helpers.CredentialsHelper;
import com.vkostenc.githubaccess.domain.repository.UsersDatabase;

public class GitHubAccessApplication extends Application{
  private static final String GITHUB_ACCESS_PRIVATE_SHARED_PREFERENCES_KEY =
      "github_access_private_shared_preferences";

  private static boolean isDebuggable = false;


  @Override
  public void onCreate() {
    super.onCreate();
    initializeRestApiTokenSharedPreferences();
    initActiveAndroid();
  }

  @SuppressWarnings("unchecked")
  private void initActiveAndroid() {
    Configuration.Builder configurationBuilder = new Configuration.Builder(this);

    configurationBuilder.addModelClass(UsersDatabase.class);

    ActiveAndroid.initialize(configurationBuilder.create());
    ActiveAndroid.setLoggingEnabled(false);
  }

  private void initializeRestApiTokenSharedPreferences() {
    CredentialsHelper.setCurrentSharedPreferences(
        getSharedPreferences(GITHUB_ACCESS_PRIVATE_SHARED_PREFERENCES_KEY,
            Context.MODE_PRIVATE));
  }
}
