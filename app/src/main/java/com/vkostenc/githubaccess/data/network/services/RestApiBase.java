package com.vkostenc.githubaccess.data.network.services;

import com.squareup.okhttp.OkHttpClient;
import java.util.concurrent.TimeUnit;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;

public class RestApiBase {

  protected RestAdapter restAdapter;

  protected RestApiBase() {
    restAdapter = buildRestAdapter();
  }

  private RestAdapter buildRestAdapter() {
    return new RestAdapter.Builder()
        .setEndpoint(ApiConstants.BASE_URL)
        .setConverter(new JacksonConverter())
        .setClient(getHttpClient())
        .build();
  }

  private Client getHttpClient() {
    OkHttpClient httpClient = new OkHttpClient();
    httpClient.setConnectTimeout(ApiConstants.HTTP_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
    httpClient.setReadTimeout(ApiConstants.HTTP_READ_TIMEOUT, TimeUnit.MILLISECONDS);
    return new OkClient(httpClient);
  }
}
