package com.vkostenc.githubaccess.data.network.helpers;

import android.content.SharedPreferences;
import android.text.TextUtils;

public class CurrentUserId {
  private static final String CURRENT_USER_LOGIN_KEY = "current_user_login";
  private static final String CURRENT_USER_NAME_KEY = "current_user_name";
  private static final String CURRENT_USER_COMPANY_KEY = "current_user_company";
  private static final String CURRENT_USER_EMAIL_KEY = "current_user_email";
  private static final String CURRENT_USER_AVATAR_URL_KEY = "current_user_avatar_url";
  private static final String CURRENT_USER_PUBLIC_REPOS_KEY = "current_user_public_repos";
  private static final String CURRENT_USER_PUBLIC_GISTS_KEY = "current_user_public_gists";
  private static final String CURRENT_USER_FOLLOWERS_KEY = "current_user_followers";
  private static final String CURRENT_USER_FOLLOWING_KEY = "current_user_following";

  private static CurrentUserId instance;
  private String login;
  private String name;
  private String company;
  private String email;
  private String avatar_url;
  private int public_repos;
  private int public_gists;
  private int followers;
  private int following;

  private SharedPreferences sharedPreferences;

  public static CurrentUserId getInstance() {
    if (instance == null) {
      synchronized (CurrentUserId.class) {
        if (instance == null) {
          instance = new CurrentUserId();
        }
      }
    }
    return instance;
  }

  public void setSharedPreferences(SharedPreferences sharedPreferences) {
    this.sharedPreferences = sharedPreferences;
  }

  public String getCurrentUserLogin() {
    String login = this.login;
    if (TextUtils.isEmpty(login)) {
      if (sharedPreferences != null) {
        login = sharedPreferences.getString(CURRENT_USER_LOGIN_KEY, "");
      }
    }
    return login;
  }

  public void setCurrentUserLogin(String login) {
    this.login = login;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString(CURRENT_USER_LOGIN_KEY, login);
      editor.apply();
    }
  }

  public String getCurrentUserName() {
    String name = this.name;
    if (TextUtils.isEmpty(name)) {
      if (sharedPreferences != null) {
        name = sharedPreferences.getString(CURRENT_USER_NAME_KEY, "");
      }
    }
    return name;
  }

  public void setCurrentUserName(String name) {
    this.name = name;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString(CURRENT_USER_NAME_KEY, name);
      editor.apply();
    }
  }

  public String getCurrentUserCompany() {
    String company = this.company;
    if (TextUtils.isEmpty(company)) {
      if (sharedPreferences != null) {
        company = sharedPreferences.getString(CURRENT_USER_COMPANY_KEY, "");
      }
    }
    return company;
  }

  public void setCurrentUserCompany(String company) {
    this.company = company;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString(CURRENT_USER_COMPANY_KEY, company);
      editor.apply();
    }
  }

  public String getCurrentUserEmail() {
    String email = this.email;
    if (TextUtils.isEmpty(email)) {
      if (sharedPreferences != null) {
        email = sharedPreferences.getString(CURRENT_USER_EMAIL_KEY, "");
      }
    }
    return email;
  }

  public void setCurrentUserEmail(String email) {
    this.email = email;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString(CURRENT_USER_EMAIL_KEY, email);
      editor.apply();
    }
  }

  public String getCurrentUserAvatarUrl() {
    String avatar_url = this.avatar_url;
    if (TextUtils.isEmpty(avatar_url)) {
      if (sharedPreferences != null) {
        avatar_url = sharedPreferences.getString(CURRENT_USER_AVATAR_URL_KEY, "");
      }
    }
    return avatar_url;
  }

  public void setCurrentUserAvatarUrl(String avatar_url) {
    this.avatar_url = avatar_url;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putString(CURRENT_USER_AVATAR_URL_KEY, avatar_url);
      editor.apply();
    }
  }

  public int getCurrentUserPublicRepos() {
    int public_repos = this.public_repos;
    if (sharedPreferences != null) {
      public_repos = sharedPreferences.getInt(CURRENT_USER_PUBLIC_REPOS_KEY, 0);
    }
    return public_repos;
  }

  public void setCurrentUserPublicRepos(int public_repos) {
    this.public_repos = public_repos;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putInt(CURRENT_USER_PUBLIC_REPOS_KEY, public_repos);
      editor.apply();
    }
  }

  public int getCurrentUserPublicGists() {
    int public_gists = this.public_gists;
    if (sharedPreferences != null) {
      public_gists = sharedPreferences.getInt(CURRENT_USER_PUBLIC_GISTS_KEY, 0);
    }
    return public_gists;
  }

  public void setCurrentUserPublicGists(int public_gists) {
    this.public_gists = public_gists;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putInt(CURRENT_USER_PUBLIC_GISTS_KEY, public_gists);
      editor.apply();
    }
  }

  public int getCurrentUserFollowers() {
    int followers = this.followers;
    if (sharedPreferences != null) {
      followers = sharedPreferences.getInt(CURRENT_USER_FOLLOWERS_KEY, 0);
    }
    return followers;
  }

  public void setCurrentUserFollowers(int followers) {
    this.followers = followers;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putInt(CURRENT_USER_FOLLOWERS_KEY, followers);
      editor.apply();
    }
  }

  public int getCurrentUserFollowing() {
    int following = this.following;
    if (sharedPreferences != null) {
      following = sharedPreferences.getInt(CURRENT_USER_FOLLOWING_KEY, 0);
    }
    return following;
  }

  public void setCurrentUserFollowing(int following) {
    this.following = following;
    if (sharedPreferences != null) {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putInt(CURRENT_USER_FOLLOWING_KEY, following);
      editor.apply();
    }
  }
}
