package com.vkostenc.githubaccess.data.models.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserEntity {

  public int id;
  public String login;
  public String name;
  public String company;
  public Date created_at;
  public Date updated_at;
  public String avatar_url;
  public String gravatar_id;
  public String blog;
  public String bio;
  public String email;
  public String location;
  public UserType type;
  public boolean site_admin;
  public int public_repos;
  public int public_gists;
  public int followers;
  public int following;

  public UserEntity() {
  }

  public UserEntity(String username) {
    this.login = username;
  }

  public enum UserType {
    User,
    Organization
  }
}
