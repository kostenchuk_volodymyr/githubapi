package com.vkostenc.githubaccess.data.network.services;

public class ApiConstants {
  public static final String BASE_URL = "https://api.github.com";
  public static final String BASE_BROWSER_URL = "https://github.com/";

  public static final int HTTP_CONNECT_TIMEOUT = 6000;
  public static final int HTTP_READ_TIMEOUT = 10000;
}
