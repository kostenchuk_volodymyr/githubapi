package com.vkostenc.githubaccess.data.network.helpers;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
public class CredentialsHelper {
  public static void setCurrentUserCredentials(UserEntity userEntity) {
    if (userEntity != null) {
      if (!TextUtils.isEmpty(userEntity.login)) {
        CurrentUserId.getInstance().setCurrentUserLogin(userEntity.login);
      }

      if (!TextUtils.isEmpty(userEntity.name)) {
        CurrentUserId.getInstance().setCurrentUserName(userEntity.name);
      }
      if (!TextUtils.isEmpty(userEntity.company)) {
        CurrentUserId.getInstance().setCurrentUserCompany(userEntity.company);
      }
      if (!TextUtils.isEmpty(userEntity.email)) {
        CurrentUserId.getInstance().setCurrentUserEmail(userEntity.email);
      }
      if (!TextUtils.isEmpty(userEntity.avatar_url)) {
        CurrentUserId.getInstance().setCurrentUserAvatarUrl(userEntity.avatar_url);
      }

      CurrentUserId.getInstance().setCurrentUserPublicRepos(userEntity.public_repos);
      CurrentUserId.getInstance().setCurrentUserPublicGists(userEntity.public_gists);
      CurrentUserId.getInstance().setCurrentUserFollowers(userEntity.followers);
      CurrentUserId.getInstance().setCurrentUserFollowing(userEntity.following);

    }
  }

  public static void logout() {
    CurrentUserId.getInstance().setCurrentUserLogin(null);
    CurrentUserId.getInstance().setCurrentUserName(null);
    CurrentUserId.getInstance().setCurrentUserCompany(null);
    CurrentUserId.getInstance().setCurrentUserEmail(null);
    CurrentUserId.getInstance().setCurrentUserAvatarUrl(null);
    CurrentUserId.getInstance().setCurrentUserPublicRepos(0);
    CurrentUserId.getInstance().setCurrentUserPublicGists(0);
    CurrentUserId.getInstance().setCurrentUserFollowers(0);
    CurrentUserId.getInstance().setCurrentUserFollowing(0);

  }

  public static void setCurrentSharedPreferences(SharedPreferences sharedPreferences) {
    CurrentUserId.getInstance().setSharedPreferences(sharedPreferences);
  }
}
