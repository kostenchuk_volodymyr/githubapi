package com.vkostenc.githubaccess.data.models.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositorysEntity {
  public RepositoryEntity [] repositoryEntities;
}
