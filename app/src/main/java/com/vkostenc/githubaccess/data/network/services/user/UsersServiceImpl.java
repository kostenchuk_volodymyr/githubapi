package com.vkostenc.githubaccess.data.network.services.user;

import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import com.vkostenc.githubaccess.data.models.repository.RepositoryEntity;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class UsersServiceImpl {

  public interface UserCallback {
    void onUserEntityLoaded(UserEntity userEntity);

    void onError(RetrofitError error);
  }

  public interface RepoCallback {
    void onRepoEntityLoaded(RepositorysEntity repositorysEntity);

    void onError(RetrofitError error);
  }

  private UsersService usersService;

  public UsersServiceImpl(RestAdapter restAdapter) {
    usersService = restAdapter.create(UsersService.class);
  }

  public void getCurrentUserRepo(String userName, RepoCallback repoCallback) {
    try {
      RepositorysEntity repositorysEntity = new RepositorysEntity();
      RepositoryEntity [] repositoryEntity = this.usersService.getGitHubUserRepos(userName);
      repositorysEntity.repositoryEntities = repositoryEntity;
      repoCallback.onRepoEntityLoaded(repositorysEntity);
    } catch (RetrofitError error) {
      repoCallback.onError(error);
    }
  }

  public void login(String userName, UserCallback userCallback) {
    try {
      UserEntity userEntity = this.usersService.getGitHubUser(userName);
      userCallback.onUserEntityLoaded(userEntity);
    } catch (RetrofitError error) {
      userCallback.onError(error);
    }
  }
}
