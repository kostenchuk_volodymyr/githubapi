package com.vkostenc.githubaccess.data.network.services.user;

import com.vkostenc.githubaccess.data.models.repository.RepositoryEntity;
import com.vkostenc.githubaccess.data.models.repository.RepositorysEntity;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface UsersService {

  @GET("/users/{user_name}")
  public UserEntity getGitHubUser(@Path("user_name") String user_name);

  @GET("/users/{user_name}/repos")
  public RepositoryEntity [] getGitHubUserRepos(@Path("user_name") String user_name);
}
