package com.vkostenc.githubaccess.data.models.repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;
import com.vkostenc.githubaccess.data.models.user.UserEntity;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryEntity {

  public boolean fork;
  @SerializedName("private") public boolean isPrivate;
  public Date created_at;
  public Date pushed_at;
  public Date updated_at;
  public int forks_count;
  public long id;
  public RepositoryEntity parent;
  public RepositoryEntity source;
  public String clone_url;
  public String description;
  public String homepage;
  public String git_url;
  public String language;
  public String default_branch;
  public String mirror_url;
  public String name;
  public String full_name;
  public String ssh_url;
  public String svn_url;
  public UserEntity owner;
  public int stargazers_count;
  public int subscribers_count;
  public int network_count;
  public int watchers_count;
  public int size;
  public int open_issues_count;
  public boolean has_issues;
  public boolean has_downloads;
  public boolean has_wiki;
  public Permissions permissions;
  public License license;
  public List<Branch> branches;
  public String archive_url;

  public class Permissions {
    public boolean admin;
    public boolean push;
    public boolean pull;
  }

  public class License {
    public String key;
    public String name;
    public String url;
    public boolean featured;
  }
}
