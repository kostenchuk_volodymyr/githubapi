package com.vkostenc.githubaccess.data.network.services;

import com.vkostenc.githubaccess.data.network.services.user.UsersServiceImpl;
import com.vkostenc.githubaccess.data.network.services.RestApiBase;

public class RestApi extends RestApiBase {

  private static RestApi instance;

  public static RestApi getInstance() {
    if (instance == null) {
      synchronized (RestApi.class) {
        if (instance == null) {
          instance = new RestApi();
        }
      }
    }
    return instance;
  }

  private RestApi() {
    super();
  }

  public UsersServiceImpl getUsersService() {
    return new UsersServiceImpl(restAdapter);
  }
}
